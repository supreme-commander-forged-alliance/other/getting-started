# Getting started

All the fundamentals that are required to get started with development for Forged Alliance Forever.

## Software

I am personally fond of using [Visual Studio Code](https://code.visualstudio.com/). It is a low-weight editor that tends to be efficient. You can download it [here](https://code.visualstudio.com/Download).

It is always useful to learn a few [hotkeys](https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf) before you start working with new software. This can easily shave off half your development time if you use your software effectively. Particularly the following hotkeys are quite useful:
 - CTRL + K + O: Open a folder (as your workspace).
 - CTRL + P: Search per file name through all files in the workspace.
 - CTRL + G: Go to a line number in the current file.
 - CTRL + F: Find (a function, for example) in the current file.
 - CTRL + H: Find and replace (refactoring, for example) in the current file.
 - SHIFT + CTRL + F: Find references (of a function, for example) in all files in the workspace.

And a few useful extensions:
 - Peacock: to change the color of the UI per workspace.
 - GitLens: to allow you to easily see who made what changes in the past.

## Gamedata

You can find all the files of the FAF repository here:
 - https://github.com/FAForever/fa

It is best to fork it and clone your fork to your disk. 

These are not all the game files - the remaining game files are part of your installation. They are part of `.scd` files that are a synonym for `.zip` files.

### Core game files:
 - `C:\Steam\steamapps\common\Supreme Commander Forged Alliance\gamedata\mohodata.scd`
 - `C:\Steam\steamapps\common\Supreme Commander Forged Alliance\gamedata\moholua.scd`
 - `C:\Steam\steamapps\common\Supreme Commander Forged Alliance\gamedata\schook.scd`

### Files of individual units, projectiles, and effects:
 - `C:\Steam\steamapps\common\Supreme Commander Forged Alliance\gamedata\effects.scd`
 - `C:\Steam\steamapps\common\Supreme Commander Forged Alliance\gamedata\units.scd`
 - `C:\Steam\steamapps\common\Supreme Commander Forged Alliance\gamedata\projectiles.scd`

Organise these files accordingly to make them easier to access when you a folder with Visual Studio Code.

## Make the game run with your changes

In order to run the game with your changes you need to initialize the game with your version of the repository. This can be done by moving the files in the bin folder to your bin directory. 

## Snippets

If you're new to Lua then working with a few pre-defined snippets may easy a lot of common mistakes. You can find them in the snippets folder.